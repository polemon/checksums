#!/usr/bin/bash

TEST_VECTORS[0]=""
TEST_VECTORS[1]="a"
TEST_VECTORS[2]="abc"
TEST_VECTORS[3]="message digest"
TEST_VECTORS[4]="abcdefghijklmnopqrstuvwxyz"
TEST_VECTORS[5]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
TEST_VECTORS[6]="12345678901234567890123456789012345678901234567890123456789012345678901234567890"

TEST_RESULTS[0]="8350e5a3e24c153df2275c9f80692773"
TEST_RESULTS[1]="32ec01ec4a6dac72c0ab96fb34c0b5d1"
TEST_RESULTS[2]="da853b0d3f88d99b30283a69e6ded6bb"
TEST_RESULTS[3]="ab4f496bfb2a530b219ff33031fe06b0"
TEST_RESULTS[4]="4e8ddff3650292ab5a4108c3aa47940b"
TEST_RESULTS[5]="da33def2a42df13975352846c30338cd"
TEST_RESULTS[6]="d5976f79d83d3a0dc9806c3c66f3efd8"

for i in {0..6}; do
    RES=$(echo -ne "${TEST_VECTORS[${i}]}" | ./md2sum.py)
    echo "assert ${i}:"
    echo "  '${TEST_VECTORS[${i}]}' --> ${TEST_RESULTS[${i}]}"
    
    if [[ "${RES}" = "${TEST_RESULTS[${i}]}" ]]; then
        echo "    >>> PASS"
    else
        echo "    >>> FAIL (result: '${RES}')"
    fi
done
