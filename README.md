# checksums

Experimental implementation of checksums

# MD2SUM

Calculates MD2 checksums as per [RFC 1319][1] (obsoleted by RFC 6149)
It implements the changes described by [Errata ID 555][2].

  [1]: https://www.ietf.org/rfc/rfc1319
  [2]: https://www.rfc-editor.org/errata/eid555 
